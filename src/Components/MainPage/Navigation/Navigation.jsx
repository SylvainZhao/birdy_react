import React from 'react';
import { fade, makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import InputBase from '@material-ui/core/InputBase';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import SearchIcon from '@material-ui/icons/Search';
import HomeIcon from '@material-ui/icons/Home';
import PersonIcon from '@material-ui/icons/Person';
import PeopleIcon from '@material-ui/icons/People';
import PowerSettingsNewIcon from '@material-ui/icons/PowerSettingsNew';
import MoreIcon from '@material-ui/icons/MoreVert';
import slogan from '../../../img/slogan.png'
import history from '../../History/History';

const useStyles = makeStyles((theme) => ({
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    display: 'none',
    [theme.breakpoints.up('sm')]: {
      display: 'block',
    },
  },
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    marginRight: theme.spacing(2),
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(3),
      width: 'auto',
    },
  },
  searchIcon: {
    padding: theme.spacing(0, 2),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputRoot: {
    color: 'inherit',
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: '40ch',
    },
  },
  sectionDesktop: {
    display: 'none',
    [theme.breakpoints.up('md')]: {
      display: 'flex',
    },
  },
  sectionMobile: {
    display: 'flex',
    [theme.breakpoints.up('md')]: {
      display: 'none',
    },
  },
}));

export default function Navigation(props) {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = React.useState(null);

  const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);

  const handleMobileMenuClose = () => {
    setMobileMoreAnchorEl(null);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
    handleMobileMenuClose();
  };

  const handleHome = () => {
      props.changeKey("home")
  }

  const handleMobileHome = () => {
      handleHome()
      handleMenuClose()
  }

  const handleProfile = () => {
    props.changeKey("profile")
  }

  const handleMobileProfile = () => {
      handleProfile()
      handleMenuClose()
  }

  const handleFriend = () => {
    props.changeKey("friend")
  }

  const handleMobileFriend = () => {
      handleFriend()
      handleMenuClose()
  }


  const handleMobileMenuOpen = (event) => {
    setMobileMoreAnchorEl(event.currentTarget);
  };

  const logout = () => {
      localStorage.removeItem("token");
      history.push({ pathname: "/login", state: "Vous vous êtes déconnecté" });
  }

  const mobileMenuId = 'primary-search-account-menu-mobile';
  const renderMobileMenu = (
    <Menu
      anchorEl={mobileMoreAnchorEl}
      anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
      id={mobileMenuId}
      keepMounted
      transformOrigin={{ vertical: 'top', horizontal: 'right' }}
      open={isMobileMenuOpen}
      onClose={handleMobileMenuClose}
    >
      <MenuItem onClick={handleMobileHome}>
        <IconButton aria-label="home" color="inherit" >
            <HomeIcon />
        </IconButton>
        <p>Accueil</p>
      </MenuItem>
      <MenuItem onClick={handleMobileProfile}>
        <IconButton aria-label="profile" color="inherit" >
          <PersonIcon />
        </IconButton>
        <p>Profil</p>
      </MenuItem>
      <MenuItem onClick={handleMobileFriend}>
        <IconButton aria-label="friend" color="inherit" >
          <PeopleIcon />
        </IconButton>
        <p>Amis</p>
      </MenuItem>
      <MenuItem onClick={logout}>
        <IconButton aria-label="shutdown" color="secondary">
            <PowerSettingsNewIcon />
        </IconButton>
        <p>Déconnexion</p>
      </MenuItem>
    </Menu>
  );

  return (
    <div className={classes.grow}>
      <AppBar position="static" color="transparent">
        <Toolbar className="container">
          <a href="/" className="d-md-inline d-none">
            <img src={slogan} height="60px" alt="Birdy" srcSet="" />
          </a>  
          <div className={classes.search}>
            <div className={classes.searchIcon}>
              <SearchIcon />
            </div>
            <InputBase
              placeholder="Recherche sur Birdy"
              classes={{
                root: classes.inputRoot,
                input: classes.inputInput,
              }}
              inputProps={{ 'aria-label': 'search' }}
            />
          </div>
          <div className={classes.grow} />
          <div className={classes.sectionDesktop}>
            <IconButton aria-label="accueil" color="inherit" onClick={handleHome}>
              
                <HomeIcon />
              
            </IconButton>
            <IconButton aria-label="profile" color="inherit" onClick={handleProfile}>
              
                <PersonIcon />
            
            </IconButton>
            <IconButton aria-label="amis" color="inherit" onClick={handleFriend}>
              <PeopleIcon />
            </IconButton>
            <IconButton edge="end" aria-label="shutdown" color="secondary" onClick={logout}>
                <PowerSettingsNewIcon />
            </IconButton>
          </div>
          <div className={classes.sectionMobile}>
            <IconButton
              aria-label="show more"
              aria-controls={mobileMenuId}
              aria-haspopup="true"
              onClick={handleMobileMenuOpen}
              color="inherit"
            >
              <MoreIcon />
            </IconButton>
          </div>
        </Toolbar>
      </AppBar>
      {renderMobileMenu}
    </div>
  );
}
