import React from 'react';
import axios from 'axios';
import Message from '../Home/Message';
import history from '../../../History/History';
import './Profile.css'

export default class Profile extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            login: '',
            lastname: '',
            name: '',
            messages: [],
            nbFollowing: 0,
            nbFollower: 0
        }
    }
    componentDidMount() {
        //get profile
        axios({
            url: '/user',
            method: 'GET',
            baseURL: 'http://localhost:8080/birdy',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8;',
                'token': localStorage.getItem("token")
            },
            params: { "login":this.props.login }
        }).then((response) => {
            if (response.data.status_code === 200) {
                this.setState({
                    login: response.data.user.login,
                    lastname: response.data.user.nom,
                    name: response.data.user.prenom
                })
                localStorage.setItem("token", response.data.token)
            } else {
                history.push({ pathname: "/login", state: "Session expiré, veuillez reconnecter." });
            }
        })
        //get messages
        axios({
            url: '/message',
            method: 'GET',
            baseURL: 'http://localhost:8080/birdy',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8;',
                'token': localStorage.getItem("token")
            },
            params: { "login":this.props.login }
        }).then((response) => {
            if (response.data.status_code === 200) {
                this.setState({
                    messages: response.data.messages
                })
                localStorage.setItem("token", response.data.token)
            } else {
                history.push({ pathname: "/login", state: "Session expiré, veuillez reconnecter." });
            }
        })
        //get following
        axios({
            url: '/friend',
            method: 'GET',
            baseURL: 'http://localhost:8080/birdy',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8;',
                'token': localStorage.getItem("token")
            },
            params: { "method": "GET_FOLLOWING", "login":this.props.login }
        }).then((response) => {
            if (response.data.status_code === 200) {
                this.setState({
                    nbFollowing: response.data.following.length
                })
                localStorage.setItem("token", response.data.token)
            } else {
                history.push({ pathname: "/login", state: "Session expiré, veuillez reconnecter." });
            }
        })
        //get follower
        axios({
            url: '/friend',
            method: 'GET',
            baseURL: 'http://localhost:8080/birdy',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8;',
                'token': localStorage.getItem("token")
            },
            params: { "method": "GET_FOLLOWER", "login":this.props.login }
        }).then((response) => {
            if (response.data.status_code === 200) {
                this.setState({
                    nbFollower: response.data.follower.length
                })
                localStorage.setItem("token", response.data.token)
            } else {
                history.push({ pathname: "/login", state: "Session expiré, veuillez reconnecter." });
            }
        })
    }
    render() {
        return (
            <div>
                <div className="Profile">
                    <h2>{this.state.name} {this.state.lastname}</h2>
                    <p className="text-secondary">{this.state.login}</p>
                    <b>{this.state.nbFollowing}</b> <span className="text-secondary">Abonnements</span> &nbsp;
                    <b>{this.state.nbFollower}</b> <span className="text-secondary">Abonnés</span>
                </div>

                {this.state.messages == null
                    ? <span>Vous n'avez aucun message non lu</span>
                    : <div>
                        {this.state.messages.map((mes) =>
                            <Message key={mes._id.$oid} id = {mes._id.$oid} 
                            username={mes.prenom + " " + mes.nom} message={mes.message} 
                            date = {mes.date}
                            login={mes.login} changeContent={this.props.changeContent}
                            changeKey={this.props.changeKey} getKey={this.props.getKey}/>)}
                    </div>}
            </div>
        )
    }
}