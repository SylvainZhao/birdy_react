import React from 'react';
import Tab from 'react-bootstrap/Tab';
import Tabs from 'react-bootstrap/Tabs'
import Following from './Following';
import Follower from './Follower';
import axios from 'axios';
import history from '../../../History/History'

export default class Friend extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            following: [],
            follower: []
        }
    }
    componentDidMount() {
        //get following
        axios({
            url: '/friend',
            method: 'GET',
            baseURL: 'http://localhost:8080/birdy',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8;',
                'token': localStorage.getItem("token")
            },
            params: { "method": "GET_FOLLOWING", "login":this.props.login }
        }).then((response) => {
            if (response.data.status_code === 200) {
                this.setState({
                    following: response.data.following
                })
                localStorage.setItem("token", response.data.token)
            } else {
                history.push({ pathname: "/login", state: "Session expiré, veuillez reconnecter." });
            }
        })
        //get follower
        axios({
            url: '/friend',
            method: 'GET',
            baseURL: 'http://localhost:8080/birdy',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8;',
                'token': localStorage.getItem("token")
            },
            params: { "method": "GET_FOLLOWER", "login":this.props.login }
        }).then((response) => {
            if (response.data.status_code === 200) {
                this.setState({
                    follower: response.data.follower
                })
                localStorage.setItem("token", response.data.token)
            } else {
                history.push({ pathname: "/login", state: "Session expiré, veuillez reconnecter." });
            }
        })
    }
    //determine if e exist in array by ID
    //This is due to problem of conference of object in memory
    equalWith(e, array){
        for(let i = 0; i < array.length; i++){
            if(array[i].idUser === e.idUser){
                return true;
            }
        }
        return false;
    }
    render() {
        return (
            <Tabs defaultActiveKey="following" id="uncontrolled-tab-example">
                <Tab eventKey="following" title="Abonnements">
                    {this.state.following == null
                        ? <span>Vous n'avez aucun abonnement.</span>
                        : <div>
                            {this.state.following.map((following) =>
                                <Following key={following.idUser} username={following.prenom + " " + following.nom} login={following.login} following={true}/>
                            )}
                        </div>}
                </Tab>
                <Tab eventKey="follower" title="Abonnés">
                    {this.state.follower == null
                        ? <span>Vous n'avez aucun abonnée.</span>
                        : <div>
                            {this.state.follower.map((follower) =>
                                <Follower key={follower.idUser} username={follower.prenom + " " + follower.nom} login={follower.login} following={this.equalWith(follower, this.state.following)}/>
                            )}
                        </div>}
                </Tab>
            </Tabs>
        )
    }
}