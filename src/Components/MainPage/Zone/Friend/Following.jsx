import React from 'react';
import Button from 'react-bootstrap/Button'
import './Friend.css'
import Snackbar from '@material-ui/core/Snackbar'
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close'
import axios from 'axios';
import history from '../../../History/History'

export default class Follower extends React.Component {
    constructor(props) {
        super(props)
        if (this.props.following) {
            this.state = {
                variant: "primary",
                status: "Abonné",
                flag: true,
                snackbar: false,
                message:""
            }
        } else {
            this.state = {
                variant: "outline-primary",
                status: "Suivre",
                flag: false,
                snackbar: false,
                message:""
            }
        }
        this.toogleFollow = this.toogleFollow.bind(this)
    }

    toogleFollow() {
        if (this.state.flag) {
            this.setState({
                variant: "outline-primary",
                status: "Suivre",
                flag: false,
                snackbar:true,
                message:"Vous vous désabonnez de cette personne"
            })
            axios({
                url: '/friend',
                method: 'DELETE',
                baseURL: 'http://localhost:8080/birdy',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8;',
                    'token': localStorage.getItem("token")
                },
                params:{"login":this.props.login}
            }).then((response) => {
                if(response.data.status_code === 204){
                    localStorage.setItem("token", response.data.token)
                }else{
                    history.push({ pathname: "/login", state: "Session expiré, veuillez reconnecter." });
                }
            })
        } else {
            this.setState({
                variant: "primary",
                status: "Abonné",
                flag: true,
                snackbar: true,
                message:"Vous vous abonnez de cette personne"
            })
            axios({
                url: '/friend',
                method: 'POST',
                baseURL: 'http://localhost:8080/birdy',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8;',
                    'token': localStorage.getItem("token")
                },
                params:{"login":this.props.login}
            }).then((response) => {
                if(response.data.status_code === 201){
                    localStorage.setItem("token", response.data.token)
                }else{
                    history.push({ pathname: "/login", state: "Session expiré, veuillez reconnecter." });
                }
            })
        }
    }

    render() {
        return (
            <div>
                <div className="follow">
                    <b>{this.props.username} </b>
                    <span className="text-secondary">{this.props.login}</span>
                    <Button onClick={this.toogleFollow} className="float-right" variant={this.state.variant}>{this.state.status}</Button>
                    <div className="clear"></div>
                </div>

                <Snackbar
                    anchorOrigin={{
                        vertical: 'bottom',
                        horizontal: 'left',
                      }}
                      open={this.state.snackbar}
                      autoHideDuration={3000}
                      onClose={()=>{ this.setState({snackbar: false})}}
                      message={this.state.message}
                      action = {
                        <IconButton size="small" aria-label="close" color="inherit" onClick={()=>{ this.setState({snackbar: false})}}>
                            <CloseIcon fontSize="small" />
                        </IconButton>
                      } 
                />
            </div>

        )
    }
}