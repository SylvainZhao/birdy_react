import React from 'react';
import Profile from '../Profile/Profile'
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close'
import DeleteIcon from '@material-ui/icons/Delete';
import './Message.css'
import Comment from './Comment';
import Link from '@material-ui/core/Link'
import Tooltip from '@material-ui/core/Tooltip'
import Snackbar from '@material-ui/core/Snackbar'
import axios from 'axios';
import history from '../../../History/History'

export default class Message extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            deleted: false,
            snackbar: false
        }
        this.checkProfile = this.checkProfile.bind(this)
        this.handleDelete = this.handleDelete.bind(this)
    }
    checkProfile(){
        //jump to tab profile
        if(this.props.login === localStorage.getItem("login")){
            if(this.props.getKey !== "profile"){
                this.props.changeKey("profile")
            }
            //or does nothing cause we re already at tab profile
        }else{
            //change the content directly of the component <Home/>
            var profile = <Profile login={this.props.login} changeKey={this.props.changeKey} changeContent={this.props.changeContent} getKey={this.props.getKey}/>
            this.props.changeContent(profile)
        }
    }

    handleDelete(){
        var id = this.props.id;
        this.setState({
            deleted: true,
            snackbar: true
        })
        axios({
            url: '/message',
            method: 'DELETE',
            baseURL: 'http://localhost:8080/birdy',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8;',
                'token': localStorage.getItem("token")
            },
            params:{"idMessage":id}
        }).then((response)=>{
            if(response.data.status_code === 204){
                localStorage.setItem("token", response.data.token)
            }else{
                history.push({ pathname: "/login", state: "Session expiré, veuillez reconnecter." });
            }
        })
    }

    dateSort(date){
        var time={};
	    var f = date.split(' ', 2);//filter space
            if(f[0].search("/") != -1){//determine contain /
                var d = (f[0] ? f[0] : '').split('/', 3);//split /
            }else {
                var d = (f[0] ? f[0] : '').split('-', 3);//split -
            }
            time.year=parseInt(d[0]);//convert to entire
            time.month=parseInt(d[1]);
            time.day=parseInt(d[2]);
            var t = (f[1] ? f[1] : '').split(':', 3);//split :
            time.hour=parseInt(t[0]);
            time.minute=parseInt(t[1]);
            time.second=parseInt(t[2]);
            return time;
    }

    render() {
        var dateObject = this.dateSort(this.props.date)
        var thisyear = new Date().getFullYear();
        var date = "";
        if (dateObject.year < thisyear){
            date = dateObject.day + "/" + dateObject.month + "/" + dateObject.year + " " + dateObject.hour + ":" + dateObject.minute 
        }else{
            date = dateObject.day + "/" + dateObject.month + " " + dateObject.hour + ":" + dateObject.minute 
        }
        return (
            <div>
                <div className="message" style={{display: this.state.deleted ? 'none' : 'block'}}>
                    <Tooltip title="Voir son profile">
                        <Link href="#" onClick={this.checkProfile} color="inherit">
                            <b>{this.props.username}</b>
                        </Link>
                    </Tooltip>
                    <span>: {this.props.message} </span>
                    <IconButton onClick={this.handleDelete} style={{ visibility: localStorage.getItem("login") === this.props.login ? 'visible' : 'hidden' }} aria-label="delete" className="float-right">
                        <DeleteIcon />
                    </IconButton>
                    <span className="text-muted float-right"> {date} </span>
                    <div className="clear"></div>
                    <Comment idMessage={this.props.id} />
                </div>
                <Snackbar
                    anchorOrigin={{
                        vertical: 'bottom',
                        horizontal: 'left',
                      }}
                      open={this.state.snackbar}
                      autoHideDuration={3000}
                      onClose={()=>{ this.setState({snackbar: false})}}
                      message="Message Supprimé"
                      action = {
                        <IconButton size="small" aria-label="close" color="inherit" onClick={()=>{ this.setState({snackbar: false})}}>
                            <CloseIcon fontSize="small" />
                        </IconButton>
                      } 
                />
            </div>
        );
    }
}