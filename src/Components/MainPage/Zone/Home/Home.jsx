import React from 'react';
import NewTweet from './NewTweet/NewTweet'
import axios from 'axios';
import Message from './Message';
import history from '../../../History/History'
import IconButton from '@material-ui/core/IconButton';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';

export default class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      messages: [],
      content: ''
    }
    this.changeContent = this.changeContent.bind(this)
  }
  componentDidMount() {
    //console.log("componentDidMount")
    axios({
      url: '/message',
      method: 'GET',
      baseURL: 'http://localhost:8080/birdy',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8;',
        'token': localStorage.getItem("token")
      }
    }).then((response) => {
      if (response.data.status_code === 200) {
        this.setState({
          messages: response.data.messages
        })
        localStorage.setItem("token", response.data.token)
      } else {
        history.push({ pathname: "/login", state: "Session expiré, veuillez reconnecter." });
      }
    })
  }

  changeContent(c) {
    this.setState({
      content: c
    })
  }
  
  render() {
    //console.log(this.state.messages)
    return (
      <div>
        {this.state.content !== ''
          ? <div>
              <IconButton onClick={() => {this.setState({content: ''})}}>
                <ArrowBackIcon />
              </IconButton>
              <hr />
              {this.state.content}
            </div>
          : <div>
              <NewTweet />
              {this.state.messages == null
                ? <span>Vous n'avez aucun message non lu</span>
                : <div>
                  {this.state.messages.map((mes) =>
                    <Message key={mes._id.$oid} id={mes._id.$oid} 
                    username={mes.prenom + " " + mes.nom} message={mes.message} 
                    date = {mes.date}
                    login={mes.login} changeContent={this.changeContent} 
                    changeKey={this.props.changeKey} getKey={this.props.getKey}/>)}
                  </div>
              }
            </div>
        }
      </div>
    );
  }
}