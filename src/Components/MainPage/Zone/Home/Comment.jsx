import React,{useState} from 'react';
import Collapse from 'react-bootstrap/Collapse';
import Input from '@material-ui/core/Input';
import IconButton from '@material-ui/core/IconButton';
import SendIcon from '@material-ui/icons/Send'
import SmsOutlinedIcon from '@material-ui/icons/SmsOutlined';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Badge from '@material-ui/core/Badge';
import { useEffect } from 'react';
import axios from 'axios';
import history from '../../../History/History'
import './Comment.css';

function Comment(props) {
    const [open, setOpen] = useState(false);
    const [comments, setComments] = useState([]);
    const [text, setText] = useState('');

    useEffect( () => {
        var idMessage = props.idMessage;
        //console.log(idMessage)
        axios({
            url: '/comment',
            method: 'GET',
            baseURL: 'http://localhost:8080/birdy',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8;',
                'token': localStorage.getItem("token")
            },
            params:{"idMessage":idMessage}
        }).then((response)=>{
            //console.log(response.data.status_code)
            if(response.data.status_code === 200){
                setComments(response.data.comments);
                localStorage.setItem("token", response.data.token)
            }else{
                history.push({ pathname: "/login", state: "Session expiré, veuillez reconnecter." });
            }
        })
    }, [])

    const handleChange = event => {
        setText(event.target.value)
    }

    const handleSubmit = () => {
        var idMessage = props.idMessage;
        axios({
            url: '/comment',
            method: 'POST',
            baseURL: 'http://localhost:8080/birdy',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8;',
                'token': localStorage.getItem("token")
            },
            params:{"idMessage":idMessage, "comment":text}
        }).then((response)=>{
            if(response.data.status_code === 201){
                localStorage.setItem("token", response.data.token)
            }else{
                history.push({ pathname: "/login", state: "Session expiré, veuillez reconnecter." });
            }
        })
    }

    return (
        <div>
            <IconButton onClick={() => setOpen(!open)} aria-controls="comment" aria-expanded={open}>
                <Badge badgeContent={comments.length} color="secondary">
                    <SmsOutlinedIcon />
                </Badge>
            </IconButton>
            <Collapse in={open}>
                <div id="comment">
                    <form onSubmit={handleSubmit} >
                        <Row>
                            <Col xs={8} md={10}>
                                <Input placeholder="Commentaire..." fullWidth={true} inputProps={{ 'aria-label': 'description' }} onChange={handleChange} />
                            </Col>
                            <IconButton aria-label="send" type = "submit" disabled = {text.length === 0}>
                                <SendIcon />
                            </IconButton>
                        </Row>
                    </form>
                    {comments.length === 0
                    ? null
                    : <div className="CommentList">
                        {comments.map((com) => 
                            <div key={com._id.$oid} className="Comment">
                                <i>{com.prenom} {com.nom}: </i>
                                <span>{com.comment}</span>
                            </div>
                        )}    
                    </div>}
                </div>
            </Collapse>
        </div>
    );
}

export default Comment;