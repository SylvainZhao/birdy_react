import React from 'react';
import './NewTweet.css';
import axios from 'axios';
import history from '../../../../History/History'

export default class NewTweet extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            chars_left: 140,
            max_char: 140,
            message:"",
            disabled:true
        }
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    handleChange = event => {
        const charCount = event.target.value.length;
        const maxChar = this.state.max_char;
        const charLength = maxChar - charCount;
        this.setState({ 
            chars_left: charLength,
            message:event.target.value });
        this.resize();
        if(charCount > 0){
            this.setState({
                disabled: false
            })
        }else{
            this.setState({
                disabled: true
            })
        }
    }
    resize() {
        if (this.refs.message) {
            this.refs.message.style.height = 'auto';
            this.refs.message.style.height = this.refs.message.scrollHeight + 'px';
        }
    }
    handleSubmit(event){
        var mes = this.state.message;
        axios({
            url: '/message',
            method: 'POST',
            baseURL: 'http://localhost:8080/birdy',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8;',
                'token': localStorage.getItem("token")
            },
            params:{"message":mes}
        }).then((response) => {
            if(response.data.status_code === 201){
                localStorage.setItem("token", response.data.token)
            }else{
                history.push({ pathname: "/login", state: "Session expiré, veuillez reconnecter." });
            }
        })
    }
    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <div className="new-message">
                    <div className="input-group input-group-lg txt-message">
                        <textarea style={{ border: "0px" }} ref="message" maxLength="140" onChange={this.handleChange} className="form-control" id="txt-message" aria-label="With textarea" placeholder="Quoi de neuf?"></textarea>
                    </div>
                    <div className="float-right">
                        <span className="text-secondary">{this.state.chars_left}/{this.state.max_char}</span> &nbsp;
                        <button className="btn btn-primary" id="btn-submit" type="submit" disabled={this.state.disabled}>Publier</button>
                    </div>
                </div>
            </form>
        )
    }
}