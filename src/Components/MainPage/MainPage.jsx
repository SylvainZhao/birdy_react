import React from 'react';
import './MainPage.css';
import Navigation from './Navigation/Navigation';
import Zone from './Zone/Zone';
import history from '../History/History'
import axios from 'axios'

export default class MainPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      login: '',
      lastname: '',
      name: '',
      loginStatus: false
    }
    this.changeKey = this.changeKey.bind(this)
    this.getKey = this.getKey.bind(this)
  }

  componentDidMount() {
    if (!localStorage.hasOwnProperty("token")) {
      history.push({ pathname: "/login", state: "Veuillez vous connecter." });
    } else {
      axios({
        url: '/user',
        method: 'GET',
        baseURL: 'http://localhost:8080/birdy',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8;',
          'token': localStorage.getItem("token")
        },
        params: { 'login': localStorage.getItem("login") }
      }).then((response) => {
        if (response.data.status_code === 200) {
          this.setState({
            login: response.data.user.login,
            lastname: response.data.user.nom,
            name: response.data.user.prenom,
            loginStatus: true
          })
          localStorage.setItem("token", response.data.token)
        } else {
          history.push({ pathname: "/login", state: "Session expiré, veuillez reconnecter." });
        }
      })
    }
  }

  changeKey(k) {
    this.refs["Zone"].changeKey(k)
  }
  getKey() {
    this.refs["Zone"].getKey()
  }

  render() {
    return (
      <div>
        {this.state.loginStatus
          ? <div>
            <Navigation changeKey={this.changeKey} getKey={this.getKey} name={this.state.name} />
            <div className="container zone">
              <Zone ref="Zone" />
            </div>
          </div>
          : null}
      </div>

    );
  }
}
