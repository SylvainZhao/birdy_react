import React from 'react'
import './Register.css'
import image from '../../img/slogan.png'
import axios from 'axios'
import Qs from 'qs'
import history from '../History/History'
import Alert from 'react-bootstrap/Alert'

export default class Register extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            email: '',
            password: '',
            lastname: '',
            name: '',
            alert: null,
            variant:"warning"
        }
        this.handleChangeEmail = this.handleChangeEmail.bind(this)
        this.handleChangePassword = this.handleChangePassword.bind(this)
        this.handleChangeLastname = this.handleChangeLastname.bind(this)
        this.handleChangeName = this.handleChangeName.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    handleChangeEmail(event){
        this.setState({
            email: event.target.value
        })
    }

    handleChangePassword(event){
        this.setState({
            password: event.target.value
        })
    }

    handleChangeLastname(event){
        this.setState({
            lastname: event.target.value
        })
    }

    handleChangeName(event){
        this.setState({
            name: event.target.value
        })
    }

    handleSubmit(event){
        let registerData = {
            "login":this.state.email,
            "password":this.state.password,
            "nom":this.state.lastname,
            "prenom":this.state.name
        }        
        event.preventDefault();
        var self = this;
        axios({
            url: '/user',
            method: 'POST',
            baseURL: 'http://localhost:8080/birdy',
            headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8;'},
            data: Qs.stringify(registerData)
        }).then(function(response){
            localStorage.setItem("token", response.data.token)
            if(response.data.status_code === 201){
                self.setState({
                    variant:"success",
                    alert:"Vous vous êtes inscrit! Redirection vers Connexion dans 2 seconds"
                })
                window.setTimeout(function() {
                    history.push("/login");
                }, 2000)
            }else{
                if(response.data.status_code === 401){
                    self.setState({
                        variant:"warning",
                        alert:"Le mot de passe doit contenir 8 caractères minimum, au moins un majuscule, un chiffre et un caractère spécial"
                    })
                }else{
                    self.setState({
                        variant:"warning",
                        alert:"Ce compte existe déjà"
                    })
                }
            }
        })
    }


    render() {
        return (
            <div className="container">
                <div className="connection-form">
                    <form className="p-5" onSubmit={this.handleSubmit}>
                        <div className="row">
                            <div className="col-md-8"><h1>Inscription à Birdy</h1></div>
                            <div className="col-md-4"> <img className="logo" src={image} alt="Birdy"></img></div>
                        </div>
                        <div className="form-group">
                            <label htmlFor="inputEmail">Email adresse</label>
                            <input type="email" className="form-control" id="inputEmail" aria-describedby="emailHelp" onChange = {this.handleChangeEmail} value = {this.state.email} required placeholder="Email adresse"/>
                            <small id="emailHelp" className="form-text text-muted">On ne partage jamais votre adresse email.</small>
                        </div>
                        <div className="form-group">
                            <label htmlFor="inputPassword">Mot de passe</label>
                            <input type="password" className="form-control" id="inputPassword" onChange = {this.handleChangePassword} value = {this.state.password} required placeholder="Mot de passe"/>
                            <small id="passwordHelp" className="form-text text-muted">8 caractères minimum, au moins un majuscule, un chiffre et un caractère spécial</small>
                        </div>
                        <div className="form-group">
                            <label htmlFor="inputLastname">Nom</label>
                            <input type="text" className="form-control" id="inputLastName" onChange = {this.handleChangeLastname} value = {this.state.lastname} required placeholder="Nom"/>
                        </div>
                        <div className="form-group">
                            <label htmlFor="inputName">Prénom</label>
                            <input type="text" className="form-control" id="inputName" onChange = {this.handleChangeName} value = {this.state.name} required placeholder="Prénom"/>
                        </div>
                        
                        <button type="submit" className="btn btn-primary">Inscription</button>
                        <hr />
                        { this.state.alert == null
                        ? null
                        : <Alert variant={this.state.variant}> {this.state.alert} </Alert>}
                    </form>
                </div>
            </div>

        );
    }
}