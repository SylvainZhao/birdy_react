import React from 'react'
import {Link} from 'react-router-dom'
import './Login.css'
import image from '../../img/slogan.png'
import axios from 'axios'
import Qs from 'qs'
import history from '../History/History'
import Alert from 'react-bootstrap/Alert'

export default class Login extends React.Component {
    constructor(props){
        super(props)
        if(localStorage.hasOwnProperty("isRemember") && localStorage.getItem("isRemember")===true){
            this.state = {
                email: localStorage.getItem("login"),
                password: localStorage.getItem("password"),
                isRemember: localStorage.getItem("isRemember"),
                alert:history.location.state
            }
        }else{
            this.state = {
                email: '',
                password: '',
                isRemember: false,
                alert:history.location.state
            }
        }
        
        this.handleChangeEmail = this.handleChangeEmail.bind(this)
        this.handleChangePassword = this.handleChangePassword.bind(this)
        this.handleClickRemember = this.handleClickRemember.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    handleChangeEmail(event){
        this.setState({
            email: event.target.value
        })
    }

    handleChangePassword(event){
        this.setState({
            password: event.target.value
        })
    }

    handleClickRemember(event){
        let remember = event.target.checked;
        this.setState({
            isRemember:remember
        })
    }


    handleSubmit(event){
        let loginData = {
            "login":this.state.email,
            "password":this.state.password
        }        
        localStorage.setItem("login",this.state.email);
        localStorage.setItem("password",this.state.password);
        localStorage.setItem("isRemember", this.state.isRemember);
        event.preventDefault();
        var self = this;
        axios({
            url: '/authentication',
            method: 'POST',
            baseURL: 'http://localhost:8080/birdy',
            headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8;'},
            data: Qs.stringify(loginData)
        }).then(function(response){
            localStorage.setItem("token", response.data.token)
            if(response.data.status_code === 200){
                history.push("/");
            }else{
                self.setState({
                    password: '',
                    alert:"Compte ou mot de passe incorrect"
                })
            }
        })
    }


    render() {
        return (
            <div className="container">
                <div className="connection-form">
                    <form className="p-5" onSubmit={this.handleSubmit}>
                        <div className="row">
                            <div className="col-md-8"><h1>Connexion à Birdy</h1></div>
                            <div className="col-md-4"> <img className="logo" src={image} alt="Birdy"></img></div>
                        </div>
                        <div className="form-group">
                            <label htmlFor="inputEmail">Email adresse</label>
                            <input type="email" className="form-control" id="inputEmail" aria-describedby="emailHelp" onChange = {this.handleChangeEmail} value = {this.state.email} required placeholder="Email adresse"/>
                            <small id="emailHelp" className="form-text text-muted">On ne partage jamais votre adresse email.</small>
                        </div>
                        <div className="form-group">
                            <label htmlFor="inputPassword">Mot de passe</label>
                            <input type="password" className="form-control" id="inputPassword" onChange = {this.handleChangePassword} value = {this.state.password} required placeholder="Mot de passe"/>
                        </div>
                        <div className="form-group form-check">
                            <input type="checkbox" className="form-check-input" id="checkBoxRemember" onClick={this.handleClickRemember}/>
                            <label className="form-check-label" htmlFor="checkBoxRemember" >Se souvenir de moi</label>
                        </div>
                        <button type="submit" className="btn btn-primary">Connexion</button>
                        <hr />
                        <Link to="/register">Inscription</Link>
                        { this.state.alert == null
                        ? null
                        : <Alert variant="warning"> {this.state.alert} </Alert>}
                    </form>
                </div>
            </div>

        );
    }
}