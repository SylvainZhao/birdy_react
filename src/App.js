import React from 'react';
import MainPage from './Components/MainPage/MainPage'
import Login from './Components/Login/Login'
import History from './Components/History/History'
import { Router, Route, Switch } from 'react-router-dom'
import Register from './Components/Register/Register';
function App() {
  return (
    <div>
      <Router history={History}>
        <Switch>
          <Route exact path="/login" component={Login} />
          <Route exact path="/register" component={Register} />
          <Route exact path="/" component={MainPage} />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
